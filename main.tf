provider "aws" {
  # Configuration options
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.21.0"

  cluster_name = "myapp-eks-cluster"
  cluster_version = "1.27"
  cluster_endpoint_public_access  = true

  subnet_ids = ["subnet-088d0ff8017164c6e","subnet-072e15a2e1b6515ee"]
  vpc_id = "vpc-073bcc743925aef58"

  tags = {
    environment = "development"
    application = "myapp"
  }

  eks_managed_node_groups = {
    dev = {
      min_size     = 1
      max_size     = 2
      desired_size = 2

      instance_types = ["t2.small"]
    }
  }
}
